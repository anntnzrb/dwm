# annt's [dwm](https://dwm.suckless.org/)'s custom build

```markdown
# dwm - dynamic window manager

dwm is an extremely fast, small, and dynamic window manager for X
```

> **suckless' [dwm git repo](https://git.suckless.org/dwm/file/README.HTML)**

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [annt's [dwm](https://dwm.suckless.org/)'s custom build](#annts-dwmhttpsdwmsucklessorgs-custom-build)
    - [patches](#patches)
    - [installation](#installation)

<!-- markdown-toc end -->

## patches

- [actualfullscreen](https://dwm.suckless.org/patches/actualfullscreen/)
- [pertag](https://dwm.suckless.org/patches/pertag/)
- [ru\_gaps](https://dwm.suckless.org/patches/ru_gaps/)
- [systray](https://dwm.suckless.org/patches/systray/)

## installation

1. `git clone` & `cd` this repo

2. install using `make`:

```sh
# make install
```
