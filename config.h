/* config.h --- dwm user configuration file */

/* helpers */
#define ARR_LEN(x) (sizeof((x)) / sizeof(*(x)))
#define LAST_IDX(x) (ARR_LEN(x) - 1)

/*
 * appearance
 */
static const char *fonts[]         = { "Iosevka:size=12" };
static const unsigned int borderpx = 2;  /* border pixel of windows */
static const unsigned int snap     = 32; /* snap pixel */
static const int showbar           = 1;  /* 0 means no bar */
static const int topbar            = 1;  /* 0 means bottom bar */
static const int gappx             = 8;  /* gaps between windows */

/* tray */
/* 0: systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systraypinning = 0;
/* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayonleft  = 0;
static const unsigned int systrayspacing = 2; /* systray spacing */
/* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int systraypinningfailfirst = 1;
static const int showsystray             = 1; /* 0 means no systray */

/*
 * colors
 *
 * use colors from pywal if available, else use defaults set here
 */
#define PYWAL "/tmp/colors-wal-dwm.h"
#if __has_include(PYWAL)
#include PYWAL
#else
static const char norm_fg[]     = "#E0E9EC";
static const char norm_bg[]     = "#000000";
static const char norm_border[] = "#000000";

static const char sel_fg[]     = "#000000";
static const char sel_bg[]     = "#D3A126";
static const char sel_border[] = "#A30B2E";

static const char *colors[][3]      = {
    /*               fg         bg         border */
    [SchemeNorm] = { norm_fg,   norm_bg,   norm_border },
    [SchemeSel]  = { sel_fg,    sel_bg,    sel_border },
};
#endif

/*
 * tags
 *
 * TAGS_STYLE:
 * 0 --- arabic numbers
 * 1 --- greek numbers
 * 2 --- roman numbers
 * 3 --- qwerty row
 */
#define TAGS_STYLE 1
#if TAGS_STYLE == 0
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
#elif TAGS_STYLE == 1
static const char *tags[] = { "α", "β", "γ", "δ", "ε", "ζ", "η", "θ", "ι" };
#elif TAGS_STYLE == 2
static const char *tags[] = { "i", "ii", "iii", "iv", "v", "vi", "vii", "viii", "xi" };
#elif TAGS_STYLE == 3
static const char *tags[] = { "q", "w", "e", "r", "t", "y", "u", "i", "o" };
#endif // TAGS_STYLE == n

static const Rule rules[] = {
    /* xprop(1):
     *	WM_CLASS(STRING) = instance, class
     *	WM_NAME(STRING) = title
     */
    /* class        instance      title   tags mask   isfloating   monitor */
    { "zoom",       NULL,         NULL,   0,          1,           -1 },
    { "mpv",        NULL,         NULL,   0,          1,           -1 },
    { "chatterino", "chatterino", NULL,   0,          1,           -1 },

    { "Microsoft Teams - Preview", NULL, NULL, 0, 1, -1 },
};

static const float mfact   = 0.5; /* factor of master area size */
static const int   nmaster = 1;   /* number of clients in master area */
/* 1 == respect size hints in tiled resizals */
static const int resizehints = 1;
/* 1 == force focus on the fullscreen window */
static const int lockfullscreen = 1;

/*
 * layouts
 *
 * first entry is default
 * no layout function means floating behavior (keep at the end)
 */
static const Layout layouts[] = {
    /* symbol   arrange function */
    { "[]=",    tile },
    { "><>",    NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define AltMask Mod1Mask
#define TAGKEYS(KEY,TAG) \
    { MODKEY,                       KEY, view,       {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask,           KEY, toggleview, {.ui = 1 << TAG} }, \
    { MODKEY|ShiftMask,             KEY, tag,        {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask|ShiftMask, KEY, toggletag,  {.ui = 1 << TAG} }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, NULL };

static Key keys[] = {
    /* modifier   key   function   argument */

    /* system */
    { MODKEY|ShiftMask, XK_r, quit, {0} },

    /* frames */
    { MODKEY,           XK_j,      focusstack,     {.i = +1 } },
    { MODKEY,           XK_k,      focusstack,     {.i = -1 } },
    { MODKEY,           XK_i,      incnmaster,     {.i = +1 } },
    { MODKEY,           XK_d,      incnmaster,     {.i = -1 } },
    { MODKEY,           XK_h,      setmfact,       {.f = -0.01} },
    { MODKEY,           XK_l,      setmfact,       {.f = +0.01} },
    { MODKEY|ShiftMask, XK_f,      togglefullscr,  {0} },
    { MODKEY|ShiftMask, XK_Tab,    zoom,           {0} },
    { MODKEY|ShiftMask, XK_q,      killclient,     {0} },

    /* gaps */
    { MODKEY|ShiftMask, XK_bracketleft,  setgaps, {.i = -5 } },
    { MODKEY|ShiftMask, XK_bracketright, setgaps, {.i = +5 } },
    { MODKEY|ShiftMask, XK_equal,        setgaps, {.i = 0  } },

    /* bar */
    { MODKEY|ShiftMask, XK_b,      togglebar,      {0} },

    /* layouts */
    { MODKEY|AltMask, XK_1,     setlayout,      {.v = &layouts[0]} },
    { MODKEY|AltMask, XK_0,     setlayout,      {.v = &layouts[LAST_IDX(layouts)]} },
    { MODKEY|AltMask, XK_space, togglefloating, {0} },

    /* monitors */
    { MODKEY|ShiftMask, XK_comma,  focusmon, {.i = -1 } },
    { MODKEY|ShiftMask, XK_period, focusmon, {.i = +1 } },
    /* { MODKEY|ShiftMask, XK_comma,  tagmon,   {.i = -1 } }, */
    /* { MODKEY|ShiftMask, XK_period, tagmon,   {.i = +1 } }, */

    /* tags */
    TAGKEYS(XK_1, 0),
    TAGKEYS(XK_2, 1),
    TAGKEYS(XK_3, 2),
    TAGKEYS(XK_4, 3),
    TAGKEYS(XK_5, 4),
    TAGKEYS(XK_6, 5),
    TAGKEYS(XK_7, 6),
    TAGKEYS(XK_8, 7),
    TAGKEYS(XK_9, 8),
    { MODKEY,           XK_0, view, {.ui = ~0 } },
    { MODKEY|ShiftMask, XK_0, tag,  {.ui = ~0 } },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
    /* click                event mask      button          function        argument */
    { ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
    { ClkWinTitle,          0,              Button2,        zoom,           {0} },
    { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
    { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
    { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
    { ClkTagBar,            0,              Button1,        view,           {0} },
    { ClkTagBar,            0,              Button3,        toggleview,     {0} },
    { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
    { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
